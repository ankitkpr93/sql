**1. SELECT**

`SELECT * FROM actor;`<br/>
`SELECT first_name FROM actor;`<br/>
`SELECT first_name, last_name FROM actor;`

**2. DISTINCT**

`SELECT DISTINCT release_year FROM film;`<br/>
`SELECT DISTINCT(release_year) FROM film;`<br/>
`SELECT DISTINCT(rental_rate) FROM film;`

**3. COUNT**

`SELECT COUNT(*) FROM payment;`<br/>
`SELECT COUNT(amount) FROM payment;`<br/>
`SELECT COUNT(DISTINCT amount) FROM payment;`

**4. WHERE**

`SELECT * FROM customer WHERE first_name = 'jared';`<br/>
`SELECT * FROM film WHERE rental_rate > 4;`<br/>
`SELECT * FROM film WHERE rental_rate > 4 AND replacement_cost >= 19.99;`<br/>
`SELECT * FROM film WHERE rental_rate > 4 AND replacement_cost >= 19.99 AND ratings = 'R';`<br/>
`SELECT * FROM film WHERE rental_rate > 4 AND replacement_cost >= 19.99 AND ratings != 'R';`

**5. ORDER BY**

`SELECT * FROM customer ORDER BY first_name;`<br/>
`SELECT * FROM customer ORDER BY first_name ASC;`<br/>
`SELECT * FROM customer ORDER BY first_name DESC;`<br/>
`SELECT * FROM customer ORDER BY store_id, first_name;`<br/>
`SELECT * FROM customer ORDER BY store_id DESC, first_name ASC;`

**6. LIMIT**

`SELECT * FROM customer LIMIT 5;`<br/>
`SELECT * FROM customer ORDER BY first_name ASC LIMIT 5;`<br/>
`SELECT * FROM payment WHERE amount != 0.00 ORDER BY payment_date DESC LIMIT 5;`

**7. BETWEEN**

`SELECT COUNT(*) FROM payment WHERE amount BETWEEN 8 AND 9;`<br/>
`SELECT COUNT(*) FROM payment WHERE amount NOT BETWEEN 8 AND 9;`<br/>
`SELECT * FROM payment WHERE payment_date BETWEEN '2007-02-01' AND '2007-02-15';`

**8. IN**

`SELECT * FROM payment WHERE amount IN (0.99, 1.98, 1.99);`<br/>
`SELECT COUNT(*) FROM payment WHERE amount IN (0.99, 1.98, 1.99);`<br/>
`SELECT COUNT(*) FROM payment WHERE amount NOT IN (0.99, 1.98, 1.99);`<br/>
`SELECT * FROM customer WHERE first_name IN ('Jake', 'John');`<br/>
`SELECT * FROM customer WHERE first_name NOT IN ('Jake', 'John');`

**9. LIKE and ILIKE**

`SELECT * FROM customer WHERE first_name LIKE 'J%';`<br/>
`SELECT COUNT(*) FROM customer WHERE first_name LIKE 'J%';`<br/>
`SELECT COUNT(*) FROM customer WHERE first_name LIKE 'J%' AND last_name LIKE 'S%';`<br/>
`SELECT COUNT(*) FROM customer WHERE first_name ILIKE 'j%' AND last_name ILIKE 's%';`<br/>
`SELECT COUNT(*) FROM customer WHERE first_name LIKE '_her%';`

**10. Aggregate Functions**

`SELECT MIN(replacement_cost) FROM film;`<br/>
`SELECT MAX(replacement_cost), MIN(replacement_cost) FROM film;`<br/>
`SELECT AVG(replacement_cost) FROM film;`<br/>
`SELECT ROUND(AVG(replacement_cost),2) FROM film;`<br/>
`SELECT SUM(replacement_cost) FROM film;`

**11. GROUP BY**

1. Group BY clause must appear after a FROM or WHERE statement.
2. In a SELECT statment, columns must either have an aggregate function or be in the GROUP BY call.
3. WHERE statment should not refer to the aggregation result. HAVING clause must be used in that case.

`SELECT customer_id, SUM(amount) FROM payment GROUP BY customer_id;`<br/>
`SELECT customer_id, SUM(amount) FROM payment GROUP BY customer_id ORDER BY SUM(amount);`<br/>
`SELECT customer_id, COUNT(amount) FROM payment GROUP BY customer_id ORDER BY COUNT(amount);`<br/>
`SELECT customer_id, staff_id, SUM(amount) FROM payment GROUP BY staff_id, customer_id ORDER BY customer_id;`<br/>
`SELECT customer_id, staff_id, SUM(amount) FROM payment GROUP BY staff_id, customer_id ORDER BY SUM(amount);`<br/>
`SELECT DATE(payment_date), SUM(amount) FROM payment GROUP BY DATE(payment_date) ORDER BY SUM(amount) DESC;`

**12. HAVING**

`SELECT customer_id, SUM(amount) FROM payment WHERE customer_id NOT IN (184,77) GROUP BY customer_id;`<br/>
`SELECT customer_id, SUM(amount) FROM payment GROUP BY customer_id HAVING SUM(amount) > 100;`<br/>
`SELECT customer_id, SUM(amount) FROM payment WHERE customer_id NOT IN (184,77) GROUP BY customer_id HAVING SUM(amount) > 100;`<br/>
`SELECT store_id, COUNT(customer_id) FROM customer GROUP BY store_id ORDER BY SUM(amount) HAVING COUNT(customer_id) > 300;`<br/>
`SELECT store_id, COUNT(customer_id) FROM customer WHERE store_id = 1 GROUP BY store_id ORDER BY SUM(amount) HAVING COUNT(customer_id) > 300;`

**13. AS**

`SELECT amount AS rental_price FROM payment;`<br/>
`SELECT SUM(amount) AS net_revenue FROM payment;`<br/>
`SELECT customer_id, SUM(amount) as total_spent FROM payment GROUP BY cutsomer_id;`<br/>
`SELECT customer_id, SUM(amount) as total_spent FROM payment GROUP BY cutsomer_id HAVING SUM(amount) > 100;`

**14. INNER JOIN**

`SELECT * FROM payment INNER JOIN customer ON payment.customer_id = customer.customer_id;`<br/>
`SELECT payment_id, payment.customer_id, first_name FROM payment INNER JOIN customer ON payment.customer_id = customer.customer_id;`

**15. FULL OUTER JOIN**

`SELECT * FROM customer FULL OUTER JOIN payment ON payment.customer_id = customer.customer_id;`<br/>
`SELECT * FROM customer FULL OUTER JOIN payment ON payment.customer_id = customer.customer_id WHERE customer.customer_id = 11;`<br/>
`SELECT payment_id, payment.customer_id, first_name FROM payment INNER JOIN customer ON payment.customer_id = customer.customer_id;`

**16. LEFT JOIN**

`SELECT film.film_id, title, inventory_id, store_id FROM film LEFT JOIN inventory on inventory.film_id = film.film_id;`<br/>
`SELECT film.film_id, title, inventory_id, store_id FROM film LEFT JOIN inventory on inventory.film_id = film.film_id WHERE film.film_id IS NULL;`

**17. RIGHT JOIN**

`SELECT film.film_id, title, inventory_id, store_id FROM film RIGHT JOIN inventory on inventory.film_id = film.film_id;`<br/>
`SELECT film.film_id, title, inventory_id, store_id FROM film RIGHT JOIN inventory on inventory.film_id = film.film_id WHERE film.film_id IS NULL;`

**18. UNION**

`SELECT film_id, inventory_id, store_id FROM film UNION SELECT film_id, inventory_id, store_id FROM film;`<br/>
`SELECT film_id, inventory_id, store_id FROM film UNION SELECT film_id, inventory_id, store_id FROM film ORDER BY film_id;`<br/>

**19. Timestamps and extract**

`SHOW TIMEZONE;`<br/>
`SELECT NOW();`<br/>
`SELECT TIMEOFDAY();`<br/>
`SELECT CURRENT_TIME;`<br/>
`SELECT CURRENT_DATE;`<br/>
`SELECT EXTRACT(YEAR FROM payment_date) AS pay_year FROM payment;`<br/>
`SELECT EXTRACT(MONTH FROM payment_date) AS pay_month FROM payment;`<br/>
`SELECT EXTRACT(QUARTER FROM payment_date) AS pay_quarter FROM payment;`<br/>
`SELECT AGE(payment_date) FROM payment;`<br/>
`SELECT TO_CHAR(payment_date,'MONTH-YYYY') FROM payment;`<br/>
`SELECT TO_CHAR(payment_date,'mon/dd/YYYY') FROM payment;`<br/>
`SELECT TO_CHAR(payment_date,'mon-dd-YYYY') FROM payment;`

**20. Mathematical functions and operators**

`SELECT rental_rate/replacement_cost FROM film;`<br/>
`SELECT ROUND(rental_rate/replacement_cost,2) FROM film;`<br/>
`SELECT ROUND(rental_rate/replacement_cost,2)*100 FROM film;`<br/>
`SELECT ROUND(rental_rate/replacement_cost,2)*100 AS percent_cost FROM film;`<br/>
`SELECT  0.1 * replacement_cost AS deposit FROM film;`

**21. String functions and operators**

`SELECT LENGTH(first_name) FROM customer;`<br/>
`SELECT first_name || last_name FROM customer;`<br/>
`SELECT first_name || " "|| last_name AS full_name FROM customer;`<br/>
`SELECT upper(first_name) || " "|| upper(last_name) AS full_name FROM customer;`<br/>
`SELECT first_name || last_name || "@gmail.com" AS email FROM customer;`<br/>
`SELECT left(first_name,1) || last_name || "@gmail.com" AS custom_email FROM customer;`

**22. SubQuery**

`SELECT * FROM film WHERE rental_rate > (SELECT AVG(rental_rate) FROM film);`<br/>
`SELECT film_id, title FROM film WHERE film_id IN (SELECT inventory.film_id FROM rental INNER JOIN inventory ON inventory.inventory_id = rental.inventory_id from rental WHERE return_date BETWEEN '2005-05-29' AND '2005-05-22') ORDER BY film_id;`<br/>
`SELECT first_name, last_name FROM customer AS c WHERE EXISTS (SELECT * FROM payment as p WHERE p.customer_id = c.customer_id AND amount > 11);`

**23. Self-Join**

`SELECT f1.title, f2.title, f1.length FROM film AS f1 INNER JOIN film as f2 ON f1.film_id != f2.film_id AND f1.length = f2.length;`

**24. CREATE**

`CREATE TABLE players(player_id SERIAL PRIMARY KEY, age SMALLINT NOT NULL);`<br/>
`CREATE TABLE account(user_id SERIAL PRIMARY KEY, username VARCHAR(50) UNIQUE NOT NULL, password VARCHAR(50) NOT NULL, email VARCHAR(100) UNIQUE NOT NULL,created_on  TIMESTAMP NOT NULL, last_login TIMESTAMP);`<br/>
`CREATE TABLE job(job_id SERIAL PRIMARY KEY, job_name VARCHAR(200) UNIQUE NOT NULL);`<br/>
`CREATE TABLE account_job(user_id INTEGER REFERENCES account(user_id), job_id INTEGER REFERENCES job(job_id),hire_date TIMESTAMP);`

**25. INSERT**

`INSERT INTO account(username,password,email,created_on) VALUES ('ANKIT','password','ankitkpr93@gmail.com',CURRENT_TIMESTAMP);`<br/>
`INSERT INTO job(job_name) VALUES ('Data Scientist');`<br/>
`INSERT INTO account_job(user_id, job_id, hire_date) VALUES (1,1,CURRENT_TIMESTAMP);`

**26. UPDATE**

`UPDATE account SET last_login = CURRENT_TIMESTAMP;`<br/>
`UPDATE account SET hire_date = account.created_on FROM account WHERE account_job.user_id = account.user_id;`<br/>
`UPDATE account SET last_login = CURRENT_TIMESTAMP RETURNING email, creted_on, last_login;`

**27. DELETE**

`DELETE FROM job WHERE job_name = 'Data Scientist' RETURNING job_id, job_name;`

**28. ALTER**

`ALTER TABLE information RENAME TO new_info;`<br/>
`ALTER TABLE new_info RENAME COLUMN person TO people;`<br/>
`ALTER TABLE new_info ALTER COLUMN people DROP NOT NULL;`<br/> 
`ALTER TABLE new_info ALTER COLUMN people SET NOT NULL;` <br/>
`ALTER TABLE new_info ALTER COLUMN people DROP UNIQUE;`

**29. DROP**

`ALTER TABLE information DROP COLUMN person;`<br/>
`ALTER TABLE information DROP COLUMN person CASCADE;`<br/>
`ALTER TABLE information DROP COLUMN IF EXISTS person;`<br/>
`ALTER TABLE information DROP COLUMN person, DROP COLUMN info;`

**30. CHECK**

`CREATE TABLE employees(emp_id SERIAL PRIMARY KEY, birthday DATE CHECK (birthday > '1900-01-01'), hire_date DATE CHECK (hire_date > birthdate));`

**31. CASE**

`SELECT customer_id, CASE WHEN customer_id <=100 THEN "Premium" WHEN customer_id BETWEEN 100 AND 200 THEN "Plus" ELSE "Normal" END AS customer_class FROM customer`

**32. CASE Expression**

`SELECT customer_id, CASE customer_id WHEN 2 THEN "Winner" WHEN 5 THEN "Second Place" ELSE "Normal" END AS raffle_results FROM customer`

**33. COALESCE**

`SELECT COALESCE(NULL,1,2,3)`<br/>
`SELECT COALESCE(discount,0)`

**34. CAST**

`SELECT CAST('5' AS INTEGER) AS new_int`<br/>
`SELECT '10'::INTEGER`<br/>
`SELECT CHAR_LENGTH(CAST(inventory_id AS VARCHAR)) AS LENGTH FROM rent`

**35. NULLIF**

`SELECT (SUM(CASE department WHEN 'A' THEN 1 ELSE 0 END)/NULLIF(SUM(CASE department WHEN 'B' THEN 1 ELSE 0 END),0)) AS department_ratio FROM depts`

**36. VIEW**

`CREATE VIEW customer_info AS SELECT first_name, last_name, address FROM customer INNER JOIN address ON customer.address_id = address.address_id`

