# SQL

In this project, I am going to write some of the SQL queries ranging from basic to complex ones. "dvdrental" database provided on the [PostgreSQL Tutorial](https://www.postgresqltutorial.com/postgresql-sample-database/) page has been used in this project.

The DVD rental database represents the business processes of a DVD rental store. The DVD rental database has many objects including:

- 15 tables
- 1 trigger
- 7 views
- 8 functions
- 1 domain
- 13 sequences

**DVD Rental ER Model**

![](dvd-rental-sample-database-diagram.png)

Please refer to [**Queries.md**](Queries.md) for project exercise.
